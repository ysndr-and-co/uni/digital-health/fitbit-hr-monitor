import { BodyPresenceSensor } from "body-presence";
import { display } from "display";
import document from "document";
import { HeartRateSensor } from "heart-rate";
import { vibration } from "haptics";


const sensors = [];
const hrmData = document.getElementById("output");
const stressData = document.getElementById("stress_output");
const background = document.getElementById("background");

const hrMeasures = []
const HR_SAMPLES = 20
const STRESS_DIFFERENCE = 10




const hrm = new HeartRateSensor({ frequency: 1 });
let stress = analyzeStress();

hrm.addEventListener("reading", () => {
  hrmData.text = hrm.heartRate ? hrm.heartRate : 0
  background.style.fill = toHEX(gradient(hrm.heartRate));

  if (hrMeasures.length > HR_SAMPLES -1) hrMeasures.shift()
  hrMeasures.push(hrm.heartRate)


  let stress_value = stress(hrMeasures);
  if (stress_value) {
    if (stress_value < STRESS_DIFFERENCE || hrm.heartRate > 100) {
      console.log("ring");
      vibration.start("ring");
      stressData.text = "Calm down :)"
    }
    else {
      stressData.text = "Stress up >:)"
    }
  }
});


sensors.push(hrm);

hrm.start();

const bps = new BodyPresenceSensor();

bps.addEventListener("reading", () => {
  if (hrm.started && !bps.present) {
    console.log("no presence.. stopping");
    hrm.stop();
  }
  if (!hrm.started && bps.present) {
    console.log("presence detected.. starting");
    hrm.start();
  }
});
sensors.push(bps);
bps.start();

display.addEventListener("change", () => {
  // Automatically stop all sensors when the screen is off to conserve battery
  display.on
    ? sensors.map(sensor => sensor.start())
    : sensors.map(sensor => sensor.stop());  
});

function analyzeStress() {

  let ignore = false;

  return (bpms) => {
    console.log(bpms)

    if (ignore) return;
    ignore = true;

    let max = 0;
    let min = Infinity;
  
    bpms.forEach((hr) => {
      if (hr > max) max = hr
      if (hr < min) min = hr
      console.log(`${max} and ${min}`)
    });
  
    let difference = max - min;
  
    console.log(`Current HR variance ${difference} in ${bpms.length} measures`);
  
    setTimeout(() => ignore = false, 1000)

    return difference

  }
}


function toHEX(color) {
  let red = Number(color.red).toString(16);
  let green = Number(color.green).toString(16);
  let blue = Number(color.blue).toString(16);

  // console.log(red)
  // console.log(green)
  // console.log(blue)


  let color = Array(red, green, blue).map(str => str.length < 2 ? "0" + str : str)

  return `#${color[0]}${color[1]}${color[2]}`;
}
function gradient(hr) {
  const colorOK = {
    red: 124,
    green: 209,
    blue: 94
  }

  const colorMedium = {
    red: 240,
    green: 133,
    blue: 51
  }

  const colorBad = {
    red: 255,
    green: 57,
    blue: 58
  }

  const min = 60;
  const max = 110;

  if (hr < min) {
    return colorOK;
  } 

  if (hr > max) {
    return colorBad;
  }
  

  hr -= min;
  let fade = (hr / ((max-min)/2)); // Max HR in range, divide by two to simulate two equally 

  // console.log(fade)

  
  let colorFrom = colorOK;
  let colorTo = colorMedium;
  if (fade >= 1) {
    fade -= 1;
    colorFrom = colorMedium;
    colorTo = colorBad;
  } 
  
  let gradient = {
    red : Math.floor(colorFrom.red + (colorTo.red - colorFrom.red) * fade),
    green : Math.floor(colorFrom.green + (colorTo.green - colorFrom.green) * fade),
    blue : Math.floor(colorFrom.blue + (colorTo.blue - colorFrom.blue) * fade)
  }

  // console.log(JSON.stringify(gradient))
  // console.log(fade)


  return gradient

}
